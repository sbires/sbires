jQuery ->
  $('<a class="modeSwitch" href="#">Afficher dans une page</a>')
    .appendTo($('main'))
    .click ->
      $(this).remove()
      $('.next, .prev').remove()
      elt = $('.simpleCarousel')

      elt.unbind('click')
        .removeClass('animate')
        .removeClass('simpleCarousel')
        .attr('style', null)
        .find('li:last-child').remove()

      clearTimeout elt.data 'timeOut'

      return false
